<?php
header('Content-Type: text/html; charset=UTF-8');

if ($_SERVER['REQUEST_METHOD'] == 'GET') {
  include('form.php');
  exit();
}

$errors=FALSE;
if (empty($_POST['name'])) {
  $errors['name']='необходимо ввести имя';
}
if(empty($_POST['email'])) {
    $errors['email']='необходимо ввести корректный e-mail';
}
if(empty($_POST['checkbox'])||$_POST['checkbox']==FALSE) {
    $errors['checkbox']='необходимо соглание';
}

if (!empty($errors)) {
  include('form.php');
  exit();
}

// Сохранение в базу данных.

try {
  $user = 'u20969';
  $pass = '8283569';
  $db = new PDO('mysql:localhost;dbname=u20969', $user, $pass, array(PDO::ATTR_PERSISTENT => true));

  $stmt = $db->prepare("INSERT INTO `bac3_person` (name,e_mail,year_of_birth,gender,num_limbs,biografy) VALUES (':name',':e_mail',str_to_date(':year_of_birth','%d-%c-%Y'),:gender,:num_limbs,':biografy')");
  $stmt -> execute(array('name'=>$_POST['name'], 'e_mail'=>$_POST['e_mail'],'year_of_birth'=>$_POST['year_of_birth'],'gender'=>$_POST ['gender'],'num_limbs'=>$_POST['numOfLimbs'],'biografy'=>$_POST['biografy']));

  $superpowers=array(
    0=>'бессмертие',
    1=>'хождение сквозь стены',
    2=>'левитация'
  );
  for($i=0;$i<3;++$i) {
    if($_POST['superpowers'][$i]!=1) continue;

    $db->prepare("INSERT INTO `bac3_list_superpower` (id_peron,id_superpower) VALUES ((select max(id) from bac3_person),(select id from bac3_superpower where type=':type')");
  $stmt -> execute(array('type'=>superpowers[$i]));
  }
} catch(PDOException $e) {
  print('Error: '.$e->getMessage());

  exit();
}
header('Location: ?save=1');
